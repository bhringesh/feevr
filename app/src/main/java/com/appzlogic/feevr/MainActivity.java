package com.appzlogic.feevr;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.projection.MediaProjectionManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.util.Pair;

import com.appzlogic.feevr.facedetection.FlirCameraSource;
import com.appzlogic.feevr.facedetection.ThermalFaceDetectionProcessor;
import com.appzlogic.feevr.graphics.GraphicOverlay;
import com.appzlogic.feevr.utils.PreferenceManager;
import com.flir.thermalsdk.ErrorCode;
import com.flir.thermalsdk.androidsdk.ThermalSdkAndroid;
import com.flir.thermalsdk.androidsdk.live.connectivity.UsbPermissionHandler;
import com.flir.thermalsdk.live.Camera;
import com.flir.thermalsdk.live.CommunicationInterface;
import com.flir.thermalsdk.live.Identity;
import com.flir.thermalsdk.live.connectivity.ConnectionStatusListener;
import com.flir.thermalsdk.live.discovery.DiscoveryEventListener;
import com.flir.thermalsdk.live.discovery.DiscoveryFactory;
import com.flir.thermalsdk.live.remote.Battery;
import com.flir.thermalsdk.live.streaming.ThermalImageStreamListener;
import com.flir.thermalsdk.log.ThermalLog;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.piasy.avconf.AvConf;
import com.piasy.avconf.ConfConfig;
import com.piasy.avconf.ConfEvents;
import com.piasy.avconf.utils.AndroidConfExtras;
import com.piasy.avconf.view.FreezeAwareRenderer;
import com.piasy.kmpp.logging.Logging;

import org.webrtc.Camera1Enumerator;
import org.webrtc.CameraVideoCapturer;
import org.webrtc.CapturerObserver;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RendererCommon;
import org.webrtc.SurfaceTextureHelper;
import org.webrtc.VideoFrame;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements /*SurfaceHolder.Callback,*/ View.OnClickListener {

    private static final String TAG = "MainActivity";
    private static final int SETTINGS_REQUEST_CODE = 1;

    private ImageView batteryImageView;
    private ImageView settings_view;
    private GraphicOverlay irImageView;

    private Identity lastIdentity;
    private FlirCameraSource cameraSource;
    private UsbPermissionHandler.UsbPermissionListener permissionListener;

    private Camera cameraInstance;
    private ConnectionStatusListener connectionListener;
    private ThermalImageStreamListener streamingListener;
    private Battery.BatteryPercentageListener percentageListener;

    /*------------------------------------------------*/

    private static final int CAPTURE_PERMISSION_REQUEST_CODE = 2;
    private android.hardware.Camera mCamera = null;

    private SurfaceView surfaceView;
    private ImageView ivSettings;
    private RelativeLayout rlMenu;
    private RelativeLayout rlSetTemp;
    private RelativeLayout rlConnectToGlass;

    private SurfaceHolder surfaceHolder;
    private FrameLayout container;

    private ImageView ivQR;
    private String roomId;
    private OwtRoomService mOwtRoomService;
    private boolean mGotPermission;
    private boolean mFirstGetRooms = true;

    private String rsUrl;
    private boolean isPublisher = true;
    private int videoWidth;
    private int videoHeight;
    private int videoFps;
    private int videoMaxBitrate;
    private int videoCodec;

    private boolean isError;

    private ConfEvents confEvents;
    private AvConf avConf;
    private String selfUid;

    private SurfaceTextureHelper cameraCaptureSurfaceTextureHelper;
    private CameraVideoCapturer cameraCapturer;
    private FreezeAwareRenderer cameraPreview;
    private FreezeAwareRenderer receiveRenderer;

    private long callStartedTimeMs = 0;
    private long callSuccessTimeMs = 0;
    private boolean establishTimeDisplayed;

    /*------------------------------------------------*/
    @TargetApi(19)
    private static int getSystemUiVisibility() {
        int flags = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            flags |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }
        return flags;
    }

    @TargetApi(17)
    private DisplayMetrics getDisplayMetrics() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager =
                (WindowManager) getApplication().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getRealMetrics(displayMetrics);
        return displayMetrics;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(getSystemUiVisibility());
        setContentView(R.layout.activity_main);

        ThermalLog.LogLevel enableLoggingInDebug = BuildConfig.DEBUG ? ThermalLog.LogLevel.DEBUG : ThermalLog.LogLevel.NONE;
        ThermalSdkAndroid.init(getApplicationContext(), enableLoggingInDebug);

        PreferenceManager.init(MainActivity.this);
        setupUiViews();
        cameraInstance = new Camera();
        cameraSource = new FlirCameraSource(this, irImageView, createFaceDetector());
        initBatteryPercentageListener();
        initPermissionListener();
        initUsbListener();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        startCameraSource(lastIdentity);
        lastIdentity = null;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (avConf != null) {
            avConf.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (avConf != null) {
            avConf.onResume();
        }
    }

    @Override
    protected void onStop() {
        lastIdentity = cameraSource.stop();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        cameraSource.release();
        super.onDestroy();
    }

    private void setupUiViews() {
        batteryImageView = (ImageView) findViewById(R.id.battery_image);
        settings_view = (ImageView) findViewById(R.id.settings_view);
        irImageView = (GraphicOverlay) findViewById(R.id.ir_image);
        ivSettings = findViewById(R.id.settings_view);
        rlMenu = findViewById(R.id.rlMenu);
        rlSetTemp = findViewById(R.id.rlSetTemp);
        rlConnectToGlass = findViewById(R.id.rlConnectToGlass);
        //surfaceView = findViewById(R.id.foregroundSurfaceView);
        ivQR = findViewById(R.id.ivQR);
        container = findViewById(R.id.container);

        /*surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);*/

        rsUrl = Constants.mRoomUrl;
        selfUid = Constants.uId;
        videoWidth = 1920;
        videoHeight = 1080;
        videoFps = 15;
        videoMaxBitrate = 2000;
        videoCodec = ConfConfig.VIDEO_CODEC_H264_BASELINE;

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.mRoomUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mOwtRoomService = retrofit.create(OwtRoomService.class);

        ivSettings.setOnClickListener(this);
        ivQR.setOnClickListener(this);
        rlConnectToGlass.setOnClickListener(this);
        rlSetTemp.setOnClickListener(this);
    }

    private ThermalFaceDetectionProcessor createFaceDetector() {
        return new ThermalFaceDetectionProcessor(getApplicationContext(), R.raw.new_beep);
    }

    private void initBatteryPercentageListener() {
        final List<Pair<Integer, Drawable>> percentageToResource = Arrays.asList(
                new Pair<Integer, Drawable>(10, getResources().getDrawable(R.drawable.ic_battery_alert, null)),
                new Pair<Integer, Drawable>(20, getResources().getDrawable(R.drawable.ic_battery_20, null)),
                new Pair<Integer, Drawable>(30, getResources().getDrawable(R.drawable.ic_battery_30, null)),
                new Pair<Integer, Drawable>(50, getResources().getDrawable(R.drawable.ic_battery_50, null)),
                new Pair<Integer, Drawable>(60, getResources().getDrawable(R.drawable.ic_battery_60, null)),
                new Pair<Integer, Drawable>(80, getResources().getDrawable(R.drawable.ic_battery_80, null)),
                new Pair<Integer, Drawable>(90, getResources().getDrawable(R.drawable.ic_battery_90, null)),
                new Pair<Integer, Drawable>(100, getResources().getDrawable(R.drawable.ic_battery_full, null))
        );

        Battery.BatteryPercentageListener percentageListener = new Battery.BatteryPercentageListener() {
            @Override
            public void onPercentageChange(int i) {
                Drawable batteryDrawable = percentageToResource.stream()
                        .filter(pair -> pair.first >= i)
                        .findFirst().get()
                        .second;

                batteryImageView.setImageDrawable(batteryDrawable);
            }
        };

        cameraSource.subscribePercentage(percentageListener);
    }

    private void initPermissionListener() {
        permissionListener = new UsbPermissionHandler.UsbPermissionListener() {
            @Override
            public void permissionGranted(@NonNull Identity identity) {
                if (!UsbPermissionHandler.isFlirOne(identity)) return;
                startCameraSource(identity);
            }

            @Override
            public void permissionDenied(@NonNull Identity identity) {

            }

            @Override
            public void error(ErrorType errorType, Identity identity) {

            }
        };
    }

    private void initUsbListener() {
        DiscoveryEventListener usbListener = new DiscoveryEventListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onCameraFound(Identity identity) {
                if (!UsbPermissionHandler.isFlirOne(identity)) return;

                if (UsbPermissionHandler.hasFlirOnePermission(identity, getApplicationContext())) {
                    startCameraSource(identity);
                } else {
                    UsbPermissionHandler usbPermissionHandler = new UsbPermissionHandler();
                    usbPermissionHandler.requestFlirOnePermisson(identity, getApplicationContext(), permissionListener);
                }
            }

            @Override
            public void onDiscoveryError(CommunicationInterface communicationInterface, ErrorCode errorCode) {
                String errMsg = "onDiscoveryError communicationInterface:" + communicationInterface + " errorCode:" + errorCode;
                Log.d(TAG, errMsg);

                // TODO: 3/17/2020 add toast message "Reconnect flir one"

                runOnUiThread(() -> {
                    // stopDiscovery();
                    // MainActivity.this.showMessage.show(errMsg);
                });
            }
        };

        DiscoveryFactory.getInstance().scan(usbListener, CommunicationInterface.USB);
    }

//    public void init() {
//        ivSettings = findViewById(R.id.ivSettings);
//        surfaceView = findViewById(R.id.foregroundSurfaceView);
//
//        surfaceHolder = surfaceView.getHolder();
//        surfaceHolder.addCallback(this);
//
//        ivSettings.setOnClickListener(this);
//    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//        if (cameraInstance != null) {
//            cameraInstance.disconnect();
//        }
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//    }
//
//    @Override
//    protected void onDestroy() {
//        cameraInstance.disconnect();
//        super.onDestroy();
//    }

    //    @Override
//    public void surfaceCreated(SurfaceHolder surfaceHolder) {
//
//        try {
//            mCamera = Camera.open(0);
//            if (mCamera == null) {
//                Toast.makeText(this, "Can not open camera",
//                        Toast.LENGTH_SHORT).show();
//                finish();
//                return;
//            }
//
//            try {
//                mCamera.setPreviewDisplay(surfaceHolder);
//            } catch (IOException e) {
//                Log.v(TAG, "SurfaceHolder is not available");
//
//                Toast.makeText(this, "SurfaceHolder is not available", Toast.LENGTH_SHORT).show();
//                finish();
//                return;
//            }
//
//            setCameraDisplayOrientation(this, 0, mCamera);
//            Camera.Parameters parameters = mCamera.getParameters();
//            mCamera.setParameters(parameters);
//            mCamera.startPreview();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
//
//    }
//
//    @Override
//    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
//
//    }
//
//    public static void setCameraDisplayOrientation(Activity activity,
//                                                   int cameraId, android.hardware.Camera camera) {
//        android.hardware.Camera.CameraInfo info =
//                new android.hardware.Camera.CameraInfo();
//        android.hardware.Camera.getCameraInfo(cameraId, info);
//        int rotation = activity.getWindowManager().getDefaultDisplay()
//                .getRotation();
//        int degrees = 0;
//        switch (rotation) {
//            case Surface.ROTATION_0:
//                degrees = 0;
//                break;
//            case Surface.ROTATION_90:
//                degrees = 90;
//                break;
//            case Surface.ROTATION_180:
//                degrees = 180;
//                break;
//            case Surface.ROTATION_270:
//                degrees = 270;
//                break;
//        }
//
//        int result;
//        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
//            result = (info.orientation + degrees) % 360;
//            result = (360 - result) % 360;  // compensate the mirror
//        } else {  // back-facing
//            result = (info.orientation - degrees + 360) % 360;
//        }
//        camera.setDisplayOrientation(result);
//    }
//
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.settings_view:
                if (rlMenu.getVisibility() == View.VISIBLE) {
                    ivSettings.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_IN);
                    rlMenu.setVisibility(View.GONE);
                } else {
                    ivSettings.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
                    rlMenu.setVisibility(View.VISIBLE);
                }
               /* Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);*/
                break;

            case R.id.rlSetTemp:
                rlMenu.setVisibility(View.GONE);
                settings_view.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_IN);
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivityForResult(intent, SETTINGS_REQUEST_CODE);
                break;

            case R.id.rlConnectToGlass:
                createRoom();
                rlMenu.setVisibility(View.GONE);
                ivSettings.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_IN);
                break;

            case R.id.ivQR:
                ivQR.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SETTINGS_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra("calibrate", false) && cameraSource != null) {
                    cameraSource.calibrate();
                }
            }
        }

        if (requestCode != CAPTURE_PERMISSION_REQUEST_CODE || resultCode != Activity.RESULT_OK) {
            return;
        } else {
            callStartedTimeMs = System.currentTimeMillis();
            startCall(data);
            startCameraPreview();
        }

    }
    @SuppressLint("MissingPermission")
    private void startCameraSource(Identity identity) {
        if (cameraSource != null && identity != null) {
            try {
                if (irImageView == null) {
                    Log.d(TAG, "resume: irImageView is null");
                }
                cameraSource.start(identity);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                cameraSource.release();
                cameraSource = null;
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void createRoom() {
        final String randrom = random();

        ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Room room = new Room();
        room.setName(randrom);
        Call<Room> call = mOwtRoomService.create(room);
        call.enqueue(new Callback<Room>() {
            @Override
            public void onResponse(final Call<Room> call, final Response<Room> response) {
                if (response.isSuccessful()) {
                    Room created = response.body();
                    roomId = created.get_id();

                    MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                    BitMatrix bitMatrix = null;
                    try {
                        bitMatrix = multiFormatWriter.encode(roomId, BarcodeFormat.QR_CODE, 400, 400);
                    } catch (WriterException e) {
                        e.printStackTrace();
                    }

                    BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                    Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                    ivQR.setImageBitmap(bitmap);
                    ivQR.setVisibility(View.VISIBLE);
                    MediaProjectionManager mediaProjectionManager =
                            (MediaProjectionManager) getSystemService(
                                    Context.MEDIA_PROJECTION_SERVICE);
                    startActivityForResult(mediaProjectionManager.createScreenCaptureIntent(),
                            CAPTURE_PERMISSION_REQUEST_CODE);

                    PreferenceManager.setStringValue(Constants.roomId, roomId);
                    /*Bundle bundle = new Bundle();
                    bundle.putString("room_id", created.get_id());
                    bundle.putString("room_name", randrom);*/
                    Log.e(TAG, "RoomId Created: " + created.get_id());

                    Log.e(TAG, created.get_id() + "   " + created.getName());

                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(final Call<Room> call, final Throwable t) {
                Toast.makeText(MainActivity.this, "create room fail: " + t.getMessage(), Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        });
    }

    public static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(16);
        char tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
//        return "990803017573703478456895409200";
    }

    public void startCall(Intent mediaProjectionPermissionResultData) {
        PeerConnectionFactory.Options options = new PeerConnectionFactory.Options();

        ConfConfig.Builder confConfigBuilder = new ConfConfig.Builder()
                .rsUrl(rsUrl)
                .mode(ConfConfig.MODE_OWT_VIDEO_ROOM_SCREEN_SHARE)
                .videoCaptureWidth(videoWidth)
                .videoCaptureHeight(videoHeight)
                .videoCaptureFps(videoFps)
                .videoMaxBitrate(videoMaxBitrate)
                .startBitrate(600)
                .videoCodec(videoCodec)
                .debugVideo(true);
        if (mediaProjectionPermissionResultData != null) {
            confConfigBuilder.extras(new AndroidConfExtras(mediaProjectionPermissionResultData));
        }

        confEvents = new ConfEvents() {
            @Override
            public void onPeerJoined(@NonNull String uid) {
                super.onPeerJoined(uid);

                if (TextUtils.equals(uid, selfUid)) {
                    return;
                }

                if (receiveRenderer != null) {
                    avConf.addRenderer(uid, receiveRenderer);
                    return;
                }

                FrameLayout wrapper = new FrameLayout(MainActivity.this);
                container.addView(wrapper, 0,
                        new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT));

                receiveRenderer =
                        new FreezeAwareRenderer(MainActivity.this, uid,
                                FreezeAwareRenderer.SCALE_TYPE_CENTER_CROP);
                wrapper.addView(receiveRenderer, ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);

                receiveRenderer.init(avConf.getRootEglBase().getEglBaseContext(), null);
                receiveRenderer.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);

                avConf.addRenderer(uid, receiveRenderer);
            }

            @Override
            public void onStreamStarted(@NonNull final String uid) {
                super.onStreamStarted(uid);

                callSuccessTimeMs = System.currentTimeMillis();
                displayEstablishTime();
            }

            @Override
            public void onStreamUpdated(@NonNull final String uid, final boolean audioActive,
                                        final boolean videoActive) {
                super.onStreamUpdated(uid, audioActive, videoActive);

                Logging.info(TAG, "onStreamUpdated " + uid
                        + ", audioActive " + audioActive
                        + ", videoActive " + videoActive);
                Logging.info(TAG, "all stream status: " + avConf.getStreamStatus());
            }

            @Override
            public void onStreamEvent(@NonNull final String uid, final int event,
                                      @NonNull final String data) {
                super.onStreamEvent(uid, event, data);

                Toast.makeText(MainActivity.this, "onStreamEvent " + uid + " " + event,
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPeerLeft(@NonNull final String uid) {
                super.onPeerLeft(uid);

                disconnect();
            }

            @Override
            public void onError(final int code, @NonNull final String data) {
                if (code == ConfEvents.ERR_RETRYING) {
                    Toast.makeText(MainActivity.this, "retrying...",
                            Toast.LENGTH_SHORT).show();
                } else {
                    if (!isError) {
                        isError = true;
                        if (code == ConfEvents.ERR_ROOM_DESTROYED) {
                            disconnect();
                        } else {
                            disconnectWithErrorMessage("onError: " + ConfEvents.errCodeName(code)
                                    + " " + data);
                        }
                    }
                }
            }
        };

        AvConf.sAppContext = getApplicationContext();
        avConf = new AvConf(selfUid, true, confConfigBuilder.build(), confEvents);

        avConf.configure(options);
        avConf.join(roomId);
    }

    private void startCameraPreview() {
        Camera1Enumerator enumerator = new Camera1Enumerator(true);
        for (String device : enumerator.getDeviceNames()) {
            if (enumerator.isFrontFacing(device)) {
                cameraCapturer = enumerator.createCapturer(device, null);
                break;
            }
        }

        if (cameraCapturer != null) {
            FrameLayout wrapper = new FrameLayout(MainActivity.this);

            container.addView(wrapper);

            DisplayMetrics displayMetrics = getDisplayMetrics();
            int horizontalMargin = 40;
            int bottomMargin = 400;
            int smallWindowWidth = (displayMetrics.widthPixels - horizontalMargin * 4) / 3;
            int smallWindowHeight = smallWindowWidth * videoWidth / videoHeight;
            FrameLayout.LayoutParams params
                    = (FrameLayout.LayoutParams) wrapper.getLayoutParams();
            params.width = smallWindowWidth;
            params.height = smallWindowHeight;
            params.gravity = Gravity.BOTTOM;
            params.bottomMargin = bottomMargin;
            params.leftMargin = horizontalMargin;
            wrapper.setLayoutParams(params);

            cameraPreview = new FreezeAwareRenderer(MainActivity.this,
                    selfUid, FreezeAwareRenderer.SCALE_TYPE_CENTER_CROP);
            /*wrapper.addView(cameraPreview, ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);*/

            cameraPreview.init(avConf.getRootEglBase().getEglBaseContext(), null);
            cameraPreview.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);
            cameraPreview.setMirror(true);

            cameraCaptureSurfaceTextureHelper = SurfaceTextureHelper.create("CameraCapture",
                    avConf.getRootEglBase().getEglBaseContext());
            cameraCapturer.initialize(cameraCaptureSurfaceTextureHelper, getApplicationContext(),
                    new CapturerObserver() {
                        @Override
                        public void onCapturerStarted(boolean b) {
                        }

                        @Override
                        public void onCapturerStopped() {
                        }

                        @Override
                        public void onFrameCaptured(VideoFrame videoFrame) {
                            cameraPreview.onFrame(videoFrame);
                        }
                    });
            cameraCapturer.startCapture(1280, 720, 30);
        }
    }

    private void displayEstablishTime() {
        if (callStartedTimeMs != 0 && callSuccessTimeMs != 0 && !establishTimeDisplayed) {
            establishTimeDisplayed = true;
            long recvSuccessDelay = callSuccessTimeMs > callStartedTimeMs
                    ? callSuccessTimeMs - callStartedTimeMs : 100;
            Toast.makeText(this, "Call success, take" + recvSuccessDelay + "ms", Toast.LENGTH_SHORT).show();
        }
    }

    // Disconnect from remote resources, dispose of local resources, and exit.
    private void disconnect() {
        if (cameraCapturer != null) {
            try {
                cameraCapturer.stopCapture();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            cameraCapturer.dispose();
            cameraCapturer = null;
        }
        if (cameraCaptureSurfaceTextureHelper != null) {
            cameraCaptureSurfaceTextureHelper.dispose();
            cameraCaptureSurfaceTextureHelper = null;
        }
        if (cameraPreview != null) {
            cameraPreview.release();
            cameraPreview = null;
        }

        if (receiveRenderer != null) {
            receiveRenderer.release();
            receiveRenderer = null;
        }

        if (avConf != null) {
            avConf.leave();
            avConf = null;
        }

        //finish();
    }

    private void disconnectWithErrorMessage(final String errorMessage) {
        new AlertDialog.Builder(this)
                .setTitle("Error")
                .setMessage(errorMessage)
                .setCancelable(false)
                .setNeutralButton("ok",
                        (dialog, id) -> {
                            dialog.cancel();
                            disconnect();
                        })
                .create()
                .show();
    }
}
