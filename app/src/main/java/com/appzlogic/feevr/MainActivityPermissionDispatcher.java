package com.appzlogic.feevr;

import androidx.core.app.ActivityCompat;

import permissions.dispatcher.PermissionUtils;

public class MainActivityPermissionDispatcher {
    public static final int REQUEST_CHECKPERMISSION = 0;

    private static final String[] PERMISSION_CHECKPERMISSION = new String[]{
            "android.permission.CAMERA",
            "android.permission.RECORD_AUDIO",
            "android.permission.MODIFY_AUDIO_SETTINGS",
            "android.permission.BLUETOOTH",
            "android.permission.WRITE_EXTERNAL_STORAGE",
            "android.permission.ACCESS_NETWORK_STATE",
            "android.permission.ACCESS_FINE_LOCATION",
            "android.permission.ACCESS_COARSE_LOCATION"};

    private MainActivityPermissionDispatcher() {
    }

    public static void checkPermissionWithPermissionCheck(SplashScreen target) {
        if (PermissionUtils.hasSelfPermissions(target, PERMISSION_CHECKPERMISSION)) {
            target.checkPermission();
        } else {
            ActivityCompat.requestPermissions(target, PERMISSION_CHECKPERMISSION, REQUEST_CHECKPERMISSION);
        }
    }

    public static void onRequestPermissionsResult(SplashScreen target, int requestCode,
                                                  int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CHECKPERMISSION:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    target.checkPermission();
                }
                break;
            default:
                break;
        }
    }
}
