package com.appzlogic.feevr;

import com.flir.thermalsdk.image.utils.UnitsConverter;

public class SettingsManager {
    private double feverThreshold = UnitsConverter.TemperatureUnit.FAHRENHEIGHT.getSiValue(98.0);
    private double scanThreshold = UnitsConverter.TemperatureUnit.FAHRENHEIGHT.getSiValue(88.5);
    private double tempOffset = UnitsConverter.TemperatureUnit.FAHRENHEIGHT.getSiValue(3.0) - UnitsConverter.TemperatureUnit.FAHRENHEIGHT.getSiValue(0.0);
    private boolean isFahrenheit = true;

    private static final double calibrationTemp = UnitsConverter.TemperatureUnit.FAHRENHEIGHT.getSiValue(92.0);

    private static SettingsManager instance;

    public static SettingsManager getInstance() {
        if (instance == null) instance = new SettingsManager();
        return instance;
    }

    private SettingsManager() { }

    public void setFeverThreshold(double feverThreshold) {
        this.feverThreshold = getTemperatureUnit().getSiValue(feverThreshold);
    }

    public double getFeverThresholdSI() {
        return feverThreshold;
    }

    public double getFeverThreshold() {
        return getTemperatureUnit().getValue(feverThreshold);
    }

    public void setScanThreshold(double scanThreshold) {
        this.scanThreshold = getTemperatureUnit().getSiValue(scanThreshold);
    }

    public double getScanThresholdSI() {
        return scanThreshold;
    }

    public double getScanThreshold() {
        return getTemperatureUnit().getValue(scanThreshold);
    }

    public double getTempOffsetSI() {
        return tempOffset;
    }

    public void setTempOffsetSi(double tempOffset) {
        this.tempOffset = tempOffset;
    }

    public void setTempOffset(double tempOffset) {
        this.tempOffset = getTemperatureUnit().getSiValue(tempOffset) - getTemperatureUnit().getSiValue(0.);
    }

    public double getTempOffset() {
        return getTemperatureUnit().getValue(tempOffset + getTemperatureUnit().getSiValue(0.));
    }

    public UnitsConverter.TemperatureUnit getTemperatureUnit() {
        return isFahrenheit ? UnitsConverter.TemperatureUnit.FAHRENHEIGHT : UnitsConverter.TemperatureUnit.CELSIUS;
    }

    public boolean getIsFahrenheit() {
        return isFahrenheit;
    }

    public void setIsFahrenheit(boolean isFahrenheit) {
        this.isFahrenheit = isFahrenheit;
    }

    public double getCalibrationTemp() { return calibrationTemp; }
}
