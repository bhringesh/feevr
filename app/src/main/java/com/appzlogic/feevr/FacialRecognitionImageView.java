package com.appzlogic.feevr;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.util.Pair;

import java.util.List;

public class FacialRecognitionImageView extends AppCompatImageView {

    public FacialRecognitionImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private List<Rect> rects;
    public void setRectangles(List<Rect> rectangles) {
        rects = rectangles;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Pair<Integer, Integer> offset = getBitmapOffset(false);
        int xOffset = offset.first;
        int yOffset = offset.second;

        if (rects != null) {
            Paint paint = new Paint();
            paint.setColor(Color.rgb(0, 0, 0));
            paint.setStrokeWidth(10);

            for (Rect rect : rects) {
                Rect offsetRect = addOffsetToRect(rect, xOffset, yOffset);
                canvas.drawRect(offsetRect, paint);
            }
        }
    }

    private Rect addOffsetToRect(Rect rect, int x, int y) {
        return new Rect(rect.left + x, rect.top + y, rect.right + x, rect.bottom + y);
    }

    private Pair<Integer, Integer> getBitmapOffset(boolean includeLayout) {
        float[] values = new float[9];

        Matrix m = getImageMatrix();
        m.getValues(values);

        Pair<Integer, Integer> offset = new Pair<Integer, Integer>(
                (int) values[Matrix.MTRANS_X],
                (int) values[Matrix.MTRANS_Y]
        );

        if (includeLayout) {
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) getLayoutParams();
            int paddingTop = (int) getPaddingTop();
            int paddingLeft = (int) getPaddingLeft();

            offset = new Pair<Integer, Integer>(
                    offset.first + paddingTop + lp.topMargin,
                    offset.second + paddingLeft + lp.leftMargin
            );
        }
        return offset;
    }
}
