package com.appzlogic.feevr;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.google.android.gms.common.util.NumberUtils;

import org.w3c.dom.Text;

import java.text.DecimalFormat;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivBack;

    private static final SettingsManager settings = SettingsManager.getInstance();

    private EditText feverThresholdEdit;
    private EditText scanThresholdEdit;
    private EditText offsetEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ivBack = findViewById(R.id.ivBack);

        ivBack.setOnClickListener(this);

//        initEditTexts();

        if (settings.getIsFahrenheit()) ((RadioButton) findViewById(R.id.radio_fahrenheit)).setChecked(true);
        else ((RadioButton) findViewById(R.id.radio_celsius)).setChecked(true);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.ivBack:
                onBackPressed();
                break;

        }
    }

    private void initEditTexts() {
//        feverThresholdEdit = (EditText) initEditText(R.id.fever_threshold_edit, (editable -> settings.setFeverThreshold(parseDouble(editable.toString(), 0.))));
//        scanThresholdEdit = (EditText) initEditText(R.id.scan_threshold_edit, (editable -> settings.setScanThreshold(parseDouble(editable.toString(), 0.))));
//        offsetEdit = (EditText) initEditText(R.id.offset_edit, (editable -> settings.setTempOffset(parseDouble(editable.toString(), 0.))));
//        updateEditTexts();
    }

    private void updateEditTexts() {
        DecimalFormat format = new DecimalFormat("#.#");
        feverThresholdEdit.setText(format.format(settings.getFeverThreshold()));
        scanThresholdEdit.setText(format.format(settings.getScanThreshold()));
        offsetEdit.setText(format.format(settings.getTempOffset()));
    }

    private static double parseDouble(String str, double defalt) {
        try {
            return Double.parseDouble(str);
        } catch (NumberFormatException e) {
            return defalt;
        }
    }

    public void onTempRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.radio_celsius:
                if (checked) settings.setIsFahrenheit(false);
                break;
            case R.id.radio_fahrenheit:
                if (checked) settings.setIsFahrenheit(true);
                break;
        }

//        if (offsetEdit != null) updateEditTexts();
    }

    public void onCalibrateButtonClicked(View view) {
        Intent i = new Intent(this, SettingsActivity.class);
        i.putExtra("calibrate", true);
        setResult(RESULT_OK, i);
        finish();
    }

    private interface AfterTextChangedListener {
        void afterTextChanged(Editable editable);
    }

    private EditText initEditText(int resource, AfterTextChangedListener after) {
        EditText edit = (EditText) findViewById(resource);
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                after.afterTextChanged(editable);
            }
        });
        return edit;
    }
}
