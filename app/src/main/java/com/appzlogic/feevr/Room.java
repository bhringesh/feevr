package com.appzlogic.feevr;

/**
 * Created by Piasy{github.com/Piasy} on 2019/3/31.
 */
public class Room {
    private String name;
    private String _id;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(final String _id) {
        this._id = _id;
    }

    @Override
    public String toString() {
        return "Room{" +
                "name='" + name + '\'' +
                ", _id='" + _id + '\'' +
                '}';
    }
}
