package com.appzlogic.feevr;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Piasy{github.com/Piasy} on 2019/3/31.
 */
public interface OwtRoomService {
    @POST("rooms")
    Call<Room> create(@Body Room room);

    @GET("rooms")
    Call<List<Room>> get(@Query("page") int page, @Query("per_page") int num);

    @DELETE("rooms/{rid}")
    Call<String> delete(@Path("rid") String rid);
}
