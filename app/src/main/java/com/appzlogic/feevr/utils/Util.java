package com.appzlogic.feevr.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created by APPZLOGIC on 6/15/2017.
 */

public class Util {
    public static Activity activity;
    public static void NormalToast(Context context, String data) {
        Toast.makeText(context, data, Toast.LENGTH_SHORT).show();
    }
    private static final String USER_AGENT = "Mozilla/5.0";

    public static String convertDateToString(Date date) {
        String dateStr = "";
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateStr = dateFormat.format(date);
        return dateStr;
    }

    public static String postHttpUrlConnection(String postInput, String requestUrl) {
        JSONObject responseObject = null;
        InputStream in = null;
        HttpURLConnection conn = null;
        String jsonString = null;
        try {
            URL url = new URL(requestUrl);

            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");


            String input = postInput;

            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                System.out.println("Inside Error" + conn.getResponseCode());

                System.out.println("Inside Error " + conn.getResponseMessage());
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            in = new BufferedInputStream(conn.getInputStream());
            jsonString = getStringFromInputStream(in);
            Log.i("JSON Response : ", jsonString);

            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        } catch (RuntimeException e) {
            jsonString = "{\"message\":\"Internal server error\",\"statusCode\":\"err001\"}";
            try {
                responseObject = new JSONObject(jsonString);

            } catch (JSONException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
        return jsonString;

    }

    public static String getHttpUrlConnection(String requestUrl) {
        JSONObject responseObject = null;
        InputStream in = null;
        HttpURLConnection conn = null;
        String jsonString = null;
        try {
            URL url = new URL(requestUrl);

            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");




            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                System.out.println("Inside Error" + conn.getResponseCode());

                System.out.println("Inside Error " + conn.getResponseMessage());
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            in = new BufferedInputStream(conn.getInputStream());
            jsonString = getStringFromInputStream(in);
            Log.i("JSON Response : ", jsonString);

            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        } catch (RuntimeException e) {
            jsonString = "{\"message\":\"Internal server error\",\"statusCode\":\"err001\"}";
            try {
                responseObject = new JSONObject(jsonString);

            } catch (JSONException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
        return jsonString;

    }

    public static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));// TODO I have
            // added charset
            // for
            // inputStreamReader
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }



    public static SharedPreferences getSharedPreferences(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("trackon", Context.MODE_PRIVATE);
        return sharedPreferences;
    }

    public static Typeface getTypeFace(Context context, int pos) {
        Typeface typeface = null;
        switch (pos) {
            case 1:
                typeface = Typeface.createFromAsset(context.getAssets(),
                        "OpenSans-Bold.ttf");
                break;
            case 2:
                typeface = Typeface.createFromAsset(context.getAssets(),
                        "OpenSans-Light.ttf");
                break;
            case 3:
                typeface = Typeface.createFromAsset(context.getAssets(),
                        "OpenSans-Regular.ttf");
                break;
        }
        return typeface;
    }
    public static byte[] getStreamByteFromImage(final File imageFile) {

        Bitmap photoBitmap = BitmapFactory.decodeFile(imageFile.getPath());
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

//        int imageRotation = getImageRotation(imageFile);
        int imageRotation = 180;

        if (imageRotation != 0)
            photoBitmap = getBitmapRotatedByDegree(photoBitmap, imageRotation);

        photoBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);

        return stream.toByteArray();
    }
    private static int getImageRotation(final File imageFile) {

        ExifInterface exif = null;
        int exifRotation = 0;

        try {
            exif = new ExifInterface(imageFile.getPath());
            exifRotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (exif == null)
            return 0;
        else
            return exifToDegrees(180);
    }

    private static int exifToDegrees(int rotation) {
        if (rotation == ExifInterface.ORIENTATION_ROTATE_90)
            return 90;
        else if (rotation == ExifInterface.ORIENTATION_ROTATE_180)
            return 180;
        else if (rotation == ExifInterface.ORIENTATION_ROTATE_270)
            return 270;

        return 0;
    }

    public static boolean isNetworkAvailable(Activity activity) {
        try{
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    private static Bitmap getBitmapRotatedByDegree(Bitmap bitmap, int rotationDegree) {
        Matrix matrix = new Matrix();
        matrix.preRotate(rotationDegree);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    private static SharedPreferences getSharedPrefrence(Context context){
        SharedPreferences preferences=context.getSharedPreferences("CTIP", Context.MODE_PRIVATE);
        return preferences;
    }
    public static boolean matchPattern(String patternText, String inputString){
        String pattern = "^"+patternText+"\\d+$";
        Pattern p = Pattern.compile(pattern);
        return p.matcher(inputString).lookingAt();
    }
}
