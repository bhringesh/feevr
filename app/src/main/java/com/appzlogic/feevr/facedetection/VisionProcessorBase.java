package com.appzlogic.feevr.facedetection;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.appzlogic.feevr.graphics.GraphicOverlay;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;

public abstract class VisionProcessorBase<TProcess, TResult> {
    private TProcess latestImage;

    private TProcess processingImage;

    public VisionProcessorBase() { }

    public void process(TProcess data, final GraphicOverlay graphics) {
        latestImage = data;
        if (processingImage == null) {
            processLatestImage(graphics);
        }
    }

    private synchronized void processLatestImage(final GraphicOverlay graphics) {
        processingImage = latestImage;
        latestImage = null;
        if (processingImage != null) {
            detectInVisionImage(processingImage, graphics);
        }
    }

    protected abstract Bitmap getBitmapFromData(TProcess data);

    private void detectInVisionImage(TProcess data, final GraphicOverlay graphics) {
        Bitmap bmp = getBitmapFromData(data);

        detectInImage(FirebaseVisionImage.fromBitmap(bmp))
            .addOnSuccessListener(results -> {
                onSuccess(data, results, graphics);
                processLatestImage(graphics);
            })
            .addOnFailureListener(e -> onFailure(e));
    }

    public void stop() { }

    protected abstract Task<TResult> detectInImage(FirebaseVisionImage image);

    protected abstract void onSuccess(
            @Nullable TProcess request,
            @NonNull TResult results,
            @NonNull GraphicOverlay graphics
    );

    protected abstract void onFailure(@NonNull Exception e);
}
