package com.appzlogic.feevr.facedetection;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.appzlogic.feevr.SettingsManager;
import com.appzlogic.feevr.graphics.CameraImageGraphic;
import com.appzlogic.feevr.graphics.GraphicOverlay;
import com.flir.thermalsdk.image.utils.UnitsConverter;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ThermalFaceDetectionProcessor extends VisionProcessorBase<ThermalImageProcessRequest, List<FirebaseVisionFace>> {

    private static final String TAG = ThermalFaceDetectionProcessor.class.getSimpleName();

    private final MediaPlayer mp;
    private boolean feverDetected = false;

    private FirebaseVisionFaceDetector detector;

    private boolean calibrationRequested = false;

    private static final SettingsManager settings = SettingsManager.getInstance();

    public ThermalFaceDetectionProcessor(Context context, int soundResource) {
        mp = MediaPlayer.create(context, soundResource);
        mp.setOnCompletionListener(mediaPlayer -> {
            if (feverDetected) {
                mediaPlayer.start();
            }
        });

        FirebaseVisionFaceDetectorOptions fastOpts = new FirebaseVisionFaceDetectorOptions.Builder()
                .setPerformanceMode(FirebaseVisionFaceDetectorOptions.FAST)
                .setLandmarkMode(FirebaseVisionFaceDetectorOptions.NO_LANDMARKS)
                .setClassificationMode(FirebaseVisionFaceDetectorOptions.NO_CLASSIFICATIONS)
                .setContourMode(FirebaseVisionFaceDetectorOptions.NO_CONTOURS)
               // .enableTracking()
                .build();
        detector = FirebaseVision.getInstance().getVisionFaceDetector(fastOpts);
    }

    @Override
    protected Bitmap getBitmapFromData(ThermalImageProcessRequest data) {
        return data.visualImage;
    }

    @Override
    protected Task<List<FirebaseVisionFace>> detectInImage(FirebaseVisionImage image) {
        return detector.detectInImage(image);
    }

    @Override
    protected void onSuccess(@Nullable ThermalImageProcessRequest request, @NonNull List<FirebaseVisionFace> faces, @NonNull GraphicOverlay graphics) {
        graphics.clear();

        if (request.visualImage != null) {
            graphics.setCameraInfo(request.visualImage.getWidth(), request.visualImage.getHeight(), 0);
        }

        if (request.thermalImage != null) {
            CameraImageGraphic imageGraphic = new CameraImageGraphic(graphics, request.thermalImage);
            graphics.add(imageGraphic);
        }

        boolean hasFever = false;

        // calibration logic
        if (calibrationRequested && faces.size() > 0) {
            calibrate(request, faces.get(0).getBoundingBox());
        }

        for (FirebaseVisionFace face : faces) {
            final double temp = request.getTemperature(face.getBoundingBox());
            if (temp >= settings.getFeverThresholdSI()) hasFever = true;

            ThermalFaceGraphic faceGraphic = new ThermalFaceGraphic(graphics, face, temp, null);
            graphics.add(faceGraphic);
        }

        feverDetected = hasFever;
        if (feverDetected && !mp.isPlaying()) {
            mp.start();
        }

        graphics.postInvalidate();
    }

    private static final double MAX_TEMP_RANGE = UnitsConverter.TemperatureUnit.FAHRENHEIGHT.getSiValue(10.0) - UnitsConverter.TemperatureUnit.FAHRENHEIGHT.getSiValue(0.0);

    private void calibrate(ThermalImageProcessRequest request, Rect faceRect) {
        double[] temps = request.getTempsInRect(faceRect);

        if (temps.length == 0) return;

        double maxTemp = Double.NEGATIVE_INFINITY;
        for (double temp : temps) {
            if (temp > maxTemp) maxTemp = temp;
        }

        double mustBeGreater = maxTemp - MAX_TEMP_RANGE;

        double[] filtered = Arrays.stream(temps).filter(temp -> temp > mustBeGreater).boxed().mapToDouble(Double::doubleValue).toArray();

        double sum = 0.0;
        int count = 0;
        for (double temp : filtered) {
            sum += temp;
            count++;
        }

        double avg = sum / count;
        double newOffset = settings.getCalibrationTemp() - avg;

        settings.setTempOffset(newOffset);

        calibrationRequested = false;
    }

    @Override
    protected void onFailure(@NonNull Exception e) {
        Log.e(TAG, "Face detection failed " + e);
    }

    @Override
    public void stop() {
        try {
            detector.close();
        } catch (IOException e) {
            Log.e(TAG, "Exception thrown while trying to close Face Detector: " + e);
        }
    }

    public void requestCalibration() {
        calibrationRequested = true;
    }
}