package com.appzlogic.feevr.facedetection;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.util.Log;
import android.view.Surface;

import com.appzlogic.feevr.SettingsManager;

import java.util.Vector;

public class ThermalImageProcessRequest {

    public Bitmap visualImage;
    public Bitmap thermalImage;
    public double[] temps;
    public int tempWidth, tempHeight;
    private static final double TO_SUM_THRESHOLD = 304.5;

    private static final String TAG = ThermalImageProcessRequest.class.getSimpleName();

    private static final SettingsManager settings = SettingsManager.getInstance();

    public ThermalImageProcessRequest(Bitmap visualImage, Bitmap thermalImage, double[] temps, int tempWidth, int tempHeight) {
        this(visualImage, thermalImage, temps, tempWidth, tempHeight, Surface.ROTATION_0);
    }

    public ThermalImageProcessRequest(Bitmap visualImage, Bitmap thermalImage, double[] temps, int tempWidth, int tempHeight, int rotation) {
        switch (rotation) {
            case Surface.ROTATION_0:
                this.visualImage = visualImage;
                this.thermalImage = thermalImage;
                this.temps = temps;
                this.tempWidth = tempHeight;
                this.tempHeight = tempWidth;
                break;
            case Surface.ROTATION_270: // have to do backwards because the camera will be facing away from the screen
                this.visualImage = rotateBitmap(visualImage, 90);
                this.thermalImage = rotateBitmap(thermalImage, 90);
                this.temps = rotateArr90Deg(temps, tempWidth, tempHeight);
                this.tempWidth = tempWidth;
                this.tempHeight = tempHeight;
                break;
            case Surface.ROTATION_90:
                this.visualImage = rotateBitmap(visualImage, 270);
                this.thermalImage = rotateBitmap(thermalImage, 270);
                this.temps = rotateArr270Deg(temps, tempWidth, tempHeight);
                this.tempWidth = tempWidth;
                this.tempHeight = tempHeight;
                break;
            default:
                Log.e(TAG, "orientation not supported: " + rotation);
        }
        Log.d(TAG, "testing");
    }

    private static Bitmap rotateBitmap(Bitmap src, int rot) {
        Matrix matrix = new Matrix();
        matrix.postRotate(rot);
        Bitmap bmp = Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
        return bmp;
    }

    private static double[] rotateArr270Deg(double[] temps, int width, int height) {
        double[] output = new double[width * height];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                output[x * height + (height - 1 - y)] = temps[y * width + x];
            }
        }
        return output;
    }

    private static double[] rotateArr90Deg(double[] temps, int width, int height) {
        double[] output = new double[width * height];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                output[(width - 1 - x) * height + y] = temps[y * width + x];
            }
        }
        return output;
    }

    public double getTemperature(Rect bounds) {
        double[] values = getTempsInRect(bounds);

        final double scanThreshold = settings.getScanThresholdSI() - settings.getTempOffsetSI();

        // avg logic
        double sum = 0.;
        int count = 0;
        for (double val : values) {
            if (val >= scanThreshold) {
                sum += val;
                count++;
            }
        }
        double avg = count > 0 ? sum / count : Double.NEGATIVE_INFINITY;
        return avg + settings.getTempOffsetSI();

//        // max logic
//        double max = Double.NEGATIVE_INFINITY;
//        for (double val : values) {
//            if (val > max) {
//                max = val;
//            }
//        }
//        return max;
    }

    public double[] getTempsInRect(Rect rect) {
        if (temps.length == 0 || tempHeight == 0 || tempWidth == 0) return new double[] {};

        Vector<Double> inRect = new Vector<Double>();
        Rect scaledRect = new Rect(
                Math.min(Math.max(rect.left * tempWidth / thermalImage.getWidth(), 0), tempWidth),
                Math.min(Math.max(rect.top * tempHeight / thermalImage.getHeight(), 0), tempHeight),
                Math.min(Math.max(rect.right * tempWidth / thermalImage.getWidth(), 0), tempWidth),
                Math.min(Math.max(rect.bottom * tempHeight / thermalImage.getHeight(), 0), tempHeight)
        );

        for (int y = scaledRect.top; y < scaledRect.bottom; y++) {
            for (int x = scaledRect.left; x < scaledRect.right; x++) {
                inRect.add(temps[y * tempWidth + x]);
            }
        }

        return inRect.stream().mapToDouble(Double::doubleValue).toArray();
    }
}
