package com.appzlogic.feevr.facedetection;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.appzlogic.feevr.SettingsManager;
import com.appzlogic.feevr.graphics.GraphicOverlay;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;

import java.text.DecimalFormat;

public class ThermalFaceGraphic extends GraphicOverlay.Graphic {
    private static final float FACE_POSITION_RADIUS = 4.0f;
    private static final float ID_TEXT_SIZE = 36.0f;
    private static final float ID_Y_OFFSET = 50.0f;
    private static final float ID_X_OFFSET = -50.0f;
    private static final float BOX_STROKE_WIDTH = 5.0f;

    private final Paint facePositionPaint;
    private final Paint idPaint;
    private final Paint boxPaint;

    private volatile FirebaseVisionFace firebaseVisionFace;

    private final Bitmap overlayBitmap;

    private double temp;

    private static final SettingsManager settings = SettingsManager.getInstance();

    public ThermalFaceGraphic(GraphicOverlay overlay, FirebaseVisionFace face, double temp, Bitmap overlayBitmap) {
        super(overlay);

        firebaseVisionFace = face;
        this.overlayBitmap = overlayBitmap;

        this.temp = temp;

        final int selectedColor = temp < settings.getScanThresholdSI() ? Color.WHITE : (temp >= settings.getFeverThresholdSI() ? Color.RED : Color.GREEN);

        facePositionPaint = new Paint();
        facePositionPaint.setColor(selectedColor);

        idPaint = new Paint();
        idPaint.setColor(selectedColor);
        idPaint.setTextSize(ID_TEXT_SIZE);

        boxPaint = new Paint();
        boxPaint.setColor(selectedColor);
        boxPaint.setStyle(Paint.Style.STROKE);
        boxPaint.setStrokeWidth(BOX_STROKE_WIDTH);
    }
    @Override
    public void draw(Canvas canvas) {
        FirebaseVisionFace face = firebaseVisionFace;
        if (face == null) {
            return;
        }

        final DecimalFormat format = new DecimalFormat("#.#" + (settings.getIsFahrenheit() ? "°F" : "°C"));
        final double adjustedTemp = settings.getTemperatureUnit().getValue(temp);
        final String formattedTemp = format.format(adjustedTemp);

        // Draws a circle at the position of the detected face, with the face's track id below.
        // An offset is used on the Y axis in order to draw the circle, face id and happiness level in the top area
        // of the face's bounding box
        float x = translateX(face.getBoundingBox().centerX());
        float y = translateY(face.getBoundingBox().centerY());
        canvas.drawCircle(x, y - 4 * ID_Y_OFFSET, FACE_POSITION_RADIUS, facePositionPaint);
        canvas.drawText("id: " + face.getTrackingId(), x + ID_X_OFFSET, y - 3 * ID_Y_OFFSET, idPaint);
        if (temp >= settings.getScanThresholdSI()) canvas.drawText("temp: " + formattedTemp, x + ID_X_OFFSET, y - 2 * ID_Y_OFFSET, idPaint);

        // Draws a bounding box around the face.
        float xOffset = scaleX(face.getBoundingBox().width() / 2.0f);
        float yOffset = scaleY(face.getBoundingBox().height() / 2.0f);
        float left = x - xOffset;
        float top = y - yOffset;
        float right = x + xOffset;
        float bottom = y + yOffset;
        canvas.drawRect(left, top, right, bottom, boxPaint);
    }
}
