package com.appzlogic.feevr.facedetection;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.display.DisplayManager;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Surface;

import androidx.annotation.RequiresPermission;

import com.appzlogic.feevr.graphics.GraphicOverlay;
import com.flir.thermalsdk.ErrorCode;
import com.flir.thermalsdk.androidsdk.image.BitmapAndroid;
import com.flir.thermalsdk.image.Rectangle;
import com.flir.thermalsdk.image.fusion.FusionMode;
import com.flir.thermalsdk.image.palettes.Palette;
import com.flir.thermalsdk.image.palettes.PaletteManager;
import com.flir.thermalsdk.live.Camera;
import com.flir.thermalsdk.live.Identity;
import com.flir.thermalsdk.live.connectivity.ConnectionStatus;
import com.flir.thermalsdk.live.connectivity.ConnectionStatusListener;
import com.flir.thermalsdk.live.remote.Battery;
import com.flir.thermalsdk.live.remote.Calibration;
import com.flir.thermalsdk.live.remote.RemoteControl;
import com.flir.thermalsdk.live.streaming.ThermalImageStreamListener;

import java.io.IOException;
import java.util.Arrays;

public class FlirCameraSource {

    protected Activity activity;
    private DisplayManager displayManager;

    private Camera camera;

    private final GraphicOverlay graphics;

    private static final String TAG = FlirCameraSource.class.getSimpleName();

    /**
     * Dedicated thread and associated runnable for calling into the detector with frames, as the
     * frames become available from the camera.
     */
    private Thread processingThread;

    private final FrameProcessingRunnable processingRunnable;

    private final Object processorLock = new Object();
    // @GuardedBy("processorLock")
    private ThermalFaceDetectionProcessor frameProcessor;

    private ConnectionStatusListener connectionListener;

    private int rotation = Surface.ROTATION_0;

    public FlirCameraSource(Activity activity, GraphicOverlay graphics, ThermalFaceDetectionProcessor frameProcessor) {
        this.activity = activity;
        this.graphics = graphics;
        this.graphics.clear();
        this.frameProcessor = frameProcessor;
        processingRunnable = new FrameProcessingRunnable();

        displayManager = (DisplayManager) activity.getSystemService(Context.DISPLAY_SERVICE);
        displayManager.registerDisplayListener(new MDisplayListener(), new Handler(Looper.getMainLooper()));
        rotation = Arrays.stream(displayManager.getDisplays()).findFirst().get().getRotation();
    }

    @RequiresPermission(Manifest.permission.CAMERA)
    public synchronized FlirCameraSource start(Identity identity) throws IOException {
        if (camera == null) {
            camera = createCamera(identity);
        }

        if (processingThread == null) {
            processingThread = new Thread(processingRunnable);
            processingRunnable.setActive(true);
            processingThread.start();
        }

        return this;
    }

    public synchronized Identity stop() {
        processingRunnable.setActive(false);
        if (processingThread != null) {
            try {
                // Wait for the thread to complete to ensure that we can't have multiple threads
                // executing at the same time (i.e., which would happen if we called start too
                // quickly after stop).
                processingThread.join();
            } catch (InterruptedException e) {
                Log.d(TAG, "Frame processing thread interrupted on release.");
            }
            processingThread = null;
        }

        if (camera != null) {
            Identity lastIdentity = camera.getIdentity();
            camera.disconnect();
            camera = null;
            return lastIdentity;
        }

        return null;
    }

    /** Stops the camera and releases the resources of the camera and underlying detector. */
    public void release() {
        synchronized (processorLock) {
            stop();
            processingRunnable.release();
            cleanScreen();

            if (frameProcessor != null) {
                frameProcessor.stop();
            }
        }
    }

    public void calibrate() {
        calibrateFlir();
        frameProcessor.requestCalibration();
    }

    private void calibrateFlir() {
        if (camera == null) return;
        RemoteControl remote = camera.getRemoteControl();
        if (remote == null) return;
        Calibration calibration = remote.getCalibration();
        if (calibration == null) return;
        calibration.nuc(); // calibrate flir camera
    }

    private Battery.BatteryPercentageListener percentageListener;

    public void subscribePercentage(Battery.BatteryPercentageListener listener) {
        percentageListener = listener;
    }

    private Camera createCamera(Identity identity) throws IOException {
        if (identity == null) {
            throw new IOException("FlirCameraSource needs an identity when creating camera");
        }

        Camera camera = new Camera();
        camera.connect(identity, new MConnectionStatusListener());

        return camera;
    }

    private class MDisplayListener implements DisplayManager.DisplayListener {

        @Override
        public void onDisplayAdded(int i) {
            rotation = displayManager.getDisplay(i).getRotation();
        }

        @Override
        public void onDisplayRemoved(int i) {

        }

        @Override
        public void onDisplayChanged(int i) {
            rotation = displayManager.getDisplay(i).getRotation();
        }
    }

    private class MConnectionStatusListener implements ConnectionStatusListener {

        @Override
        public void onConnectionStatusChanged(ConnectionStatus status, ErrorCode errorCode) {
            switch (status) {
                case CONNECTED:
                    camera.subscribeStream(new MThermalImageStreamingListener());

                    if (percentageListener != null) {
                        RemoteControl remoteControl = camera.getRemoteControl();
                        if (remoteControl != null) {
                            Battery battery = remoteControl.getBattery();
                            if (battery != null) {
                                try {
                                    battery.subscribePercentage(percentageListener);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    break;
                case DISCONNECTED:
                    // TODO: 3/17/2020 add toast message "Reconnect flir one"
                    Log.d(TAG, "Flir camera disconnected");
                    stop();
                    break;
            }
        }
    }

    // ==============================================================================================
    // Frame processing
    // ==============================================================================================

    private class MThermalImageStreamingListener implements ThermalImageStreamListener {

        private final Palette palette = PaletteManager.getDefaultPalettes().get(0);

        @Override
        public void onImageReceived() {
            camera.withImage(this, thermalImage -> {
                Bitmap thermalBmp;
                {
                    thermalImage.setPalette(palette);
                    thermalImage.getFusion().setFusionMode(FusionMode.MSX);
                    thermalBmp = BitmapAndroid.createBitmap(thermalImage.getImage()).getBitMap();
                }

                double cropScale = 0.095;

                Bitmap visualBmp = BitmapAndroid.createBitmap(thermalImage.getFusion().getPhoto()).getBitMap();
                Bitmap visualCroppedBmp = Bitmap.createBitmap(visualBmp, (int) (visualBmp.getWidth() * cropScale), (int) (visualBmp.getHeight() * cropScale), (int) (visualBmp.getWidth() * (1. - cropScale * 2)), (int) (visualBmp.getHeight() * (1. - cropScale * 2)));
                Bitmap visualScaledBmp = Bitmap.createScaledBitmap(visualCroppedBmp, 360, 480, false); // shrink image for better performance

                double[] values = thermalImage.getValues(new Rectangle(0, 0, thermalImage.getWidth(), thermalImage.getHeight()));

                ThermalImageProcessRequest req = new ThermalImageProcessRequest(visualScaledBmp, thermalBmp, values, thermalImage.getWidth(), thermalImage.getHeight(), rotation);
                processingRunnable.setNextFrame(req);
            });
        }
    }

    /**
     * This runnable controls access to the underlying receiver, calling it to process frames when
     * available from the camera. This is designed to run detection on frames as fast as possible
     * (i.e., without unnecessary context switching or waiting on the next frame).
     *
     * <p>While detection is running on a frame, new frames may be received from the camera. As these
     * frames come in, the most recent frame is held onto as pending. As soon as detection and its
     * associated processing is done for the previous frame, detection on the mostly recently received
     * frame will immediately start on the same thread.
     */
    private class FrameProcessingRunnable implements Runnable {

        // This lock guards all of the member variables below.
        private final Object lock = new Object();
        private boolean active = true;

        // These pending variables hold the state associated with the new frame awaiting processing.
        private ThermalImageProcessRequest pendingFrameData;

        FrameProcessingRunnable() {}

        /**
         * Releases the underlying receiver. This is only safe to do after the associated thread has
         * completed, which is managed in camera source's release method above.
         */
        @SuppressLint("Assert")
        void release() {
            assert (processingThread.getState() == Thread.State.TERMINATED);
        }

        /** Marks the runnable as active/not active. Signals any blocked threads to continue. */
        void setActive(boolean active) {
            synchronized (lock) {
                this.active = active;
                lock.notifyAll();
            }
        }

        /**
         * Sets the frame data received from the camera. This adds the previous unused frame buffer (if
         * present) back to the camera, and keeps a pending reference to the frame data for future use.
         */
        void setNextFrame(ThermalImageProcessRequest data) {
            synchronized (lock) {
                if (pendingFrameData != null) {
                    pendingFrameData = null;
                }

//                if (!bytesToByteBuffer.containsKey(data)) {
//                    Log.d(
//                            TAG,
//                            "Skipping frame. Could not find ByteBuffer associated with the image "
//                                    + "data from the camera.");
//                    return;
//                }

                pendingFrameData = data; //bytesToByteBuffer.get(data);

                // Notify the processor thread if it is waiting on the next frame (see below).
                lock.notifyAll();
            }
        }

        /**
         * As long as the processing thread is active, this executes detection on frames continuously.
         * The next pending frame is either immediately available or hasn't been received yet. Once it
         * is available, we transfer the frame info to local variables and run detection on that frame.
         * It immediately loops back for the next frame without pausing.
         *
         * <p>If detection takes longer than the time in between new frames from the camera, this will
         * mean that this loop will run without ever waiting on a frame, avoiding any context switching
         * or frame acquisition time latency.
         *
         * <p>If you find that this is using more CPU than you'd like, you should probably decrease the
         * FPS setting above to allow for some idle time in between frames.
         */
        @SuppressLint("InlinedApi")
        @SuppressWarnings("GuardedBy")
        @Override
        public void run() {
            ThermalImageProcessRequest data;

            while (true) {
                synchronized (lock) {
                    while (active && (pendingFrameData == null)) {
                        try {
                            // Wait for the next frame to be received from the camera, since we
                            // don't have it yet.
                            lock.wait();
                        } catch (InterruptedException e) {
                            Log.d(TAG, "Frame processing loop terminated.", e);
                            return;
                        }
                    }

                    if (!active) {
                        // Exit the loop once this camera source is stopped or released.  We check
                        // this here, immediately after the wait() above, to handle the case where
                        // setActive(false) had been called, triggering the termination of this
                        // loop.
                        return;
                    }

                    // Hold onto the frame data locally, so that we can use this for detection
                    // below.  We need to clear pendingFrameData to ensure that this buffer isn't
                    // recycled back to the camera before we are done using that data.
                    data = pendingFrameData;
                    pendingFrameData = null;
                }

                // The code below needs to run outside of synchronization, because this will allow
                // the camera to add pending frame(s) while we are running detection on the current
                // frame.

                try {
                    synchronized (processorLock) {
                        Log.d(TAG, "Process an image");
                        frameProcessor.process(
                                data,
                                graphics);
                    }
                } catch (Exception t) {
                    Log.e(TAG, "Exception thrown from receiver.", t);
                } //finally {
                    //camera.addCallbackBuffer(data.array());
                //}
            }
        }
    }

    /** Cleans up graphicOverlay and child classes can do their cleanups as well . */
    private void cleanScreen() {
        graphics.clear();
    }
}
