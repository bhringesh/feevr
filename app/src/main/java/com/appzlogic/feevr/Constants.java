package com.appzlogic.feevr;

import java.util.Random;

public class Constants {
    public static final String mRoomUrl = "http://ec2-18-188-127-27.us-east-2.compute.amazonaws.com:3001";
    public static final String uId = String.valueOf(new Random(System.currentTimeMillis()).nextInt(Integer.MAX_VALUE));
    public static final String resolution = "1920 x 1080";
    public static final String fps = "Default";
    public static final String bitrateType = "Manual";
    public static final String bitrateValue = "2000";
    public static final String videoCodec = "H264 Baseline";
    public static final String roomId = "roomId";
    public static final String isStarted = "started";

    public static final String xAxis = "xAxis";
    public static final String yAxis = "yAxis";
    public static final String scale = "scale";
    public static final String deviceName = "SWORD";
    public static final String bluetoothDeviceName = "bluetoothDeviceName";
    public static final String camera = "0";
}
