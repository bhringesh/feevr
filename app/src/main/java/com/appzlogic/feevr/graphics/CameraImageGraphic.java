package com.appzlogic.feevr.graphics;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;

import com.appzlogic.feevr.Vector2;

/** Draw camera image to background. */
public class CameraImageGraphic extends GraphicOverlay.Graphic {

    private final GraphicOverlay overlay;
    private final Bitmap bitmap;

    public CameraImageGraphic(GraphicOverlay overlay, Bitmap bitmap) {
        super(overlay);
        this.overlay = overlay;
        this.bitmap = bitmap;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawBitmap(bitmap, null, new RectF(translateX(0), translateY(0), translateX(bitmap.getWidth()), translateY(bitmap.getHeight())), null);
    }
}
