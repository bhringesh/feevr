package com.appzlogic.feevr;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.piasy.avconf.AvConf;

import permissions.dispatcher.NeedsPermission;

public class SplashScreen extends AppCompatActivity {
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    private boolean mGotPermission;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        MainActivityPermissionDispatcher.checkPermissionWithPermissionCheck(SplashScreen.this);

        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
        } else {
            new Handler().postDelayed(new Runnable(){
                @Override
                public void run() {
                    //Create an Intent that will start the Menu-Activity.
                    Intent mainIntent = new Intent(SplashScreen.this,MainActivity.class);
                    startActivity(mainIntent);
                    finish();
                }
            }, 2000);
        }

    }

   /* @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run() {
                        *//* Create an Intent that will start the Menu-Activity. *//*
                        Intent mainIntent = new Intent(SplashScreen.this,MainActivity.class);
                        startActivity(mainIntent);
                        finish();
                    }
                }, 2000);
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }*/

    @SuppressLint("NeedOnRequestPermissionsResult")
    @Override
    public void onRequestPermissionsResult(final int requestCode,
                                           @NonNull final String[] permissions,
                                           @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MainActivityPermissionDispatcher.onRequestPermissionsResult(SplashScreen.this, requestCode,
                grantResults);

    }

    @NeedsPermission({
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.MODIFY_AUDIO_SETTINGS,
            Manifest.permission.BLUETOOTH,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    })
    public void checkPermission() {
        mGotPermission = true;
        if (!AvConf.initialize(this, true)) {
            Toast.makeText(this, "initialize fail", Toast.LENGTH_LONG).show();
        }
    }
}
